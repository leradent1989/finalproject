package dan.it.step.booking.dao;

import dan.it.step.booking.domain.Booking;
import dan.it.step.booking.domain.Flight;
import dan.it.step.booking.domain.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class BookingDao {

    private List <Booking> bookingList;


    public BookingDao(List <Booking> bookingList) {

        this.bookingList = bookingList;

    }

    public List<Booking> getBookingList() {
        return bookingList;
    }

    public void setBookingList(List<Booking> bookingList) {
        this.bookingList = bookingList;
    }



    public void saveBookingList(List<Booking> bookings){
        bookingList = bookings;
    }


    public List<Booking> getBookingListFromDb(){

        List <Booking> bookings;
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("listBookings") ) ) {
            bookings = (List<Booking>) objectInputStream.readObject();

        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return bookings;
    }

    public void loadData (){
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("listBookings"))) {
            objectOutputStream.writeObject(bookingList);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Booking addBooking(List <User> passengers, Flight flight){


        int passengerNumber = passengers.size();
      Booking booking =  new Booking(passengers,passengerNumber,flight);

        bookingList.add(booking);

                passengers.forEach(el->el.getBooking().add(booking));

        flight.setFreeSeats(flight.getFreeSeats() -1);

        passengers.forEach(el-> flight.getPassengers().add(el));
        return  booking;
    }

    public boolean deleteBooking (int bookingId ){
        Booking booking = bookingList
                .stream()
                .filter(el -> el.getId() == bookingId)
                .findFirst()
                .orElse(null);
        if (booking==null) return false;
        bookingList.remove(booking);
        booking.getPassengerList().forEach(el -> el.getBooking().remove(booking));

        Flight flight = booking.getFlight();

        flight.setFreeSeats(flight.getFreeSeats() + 1);

        booking.getPassengerList().forEach(el-> {

            flight.getPassengers().remove(el);

            el.getBooking().remove(booking);});
        return true;
    }


    public List<Booking>  findAll() {
        return bookingList;

    }


        public List<Booking> checkPassenegersFlighs(String name, String surname){
        List<Booking> passengersFlights = new ArrayList<>();
        for (int i=0; i<bookingList.size();i++){
            for(int k =0; k< bookingList.get(i).getPassengerList().size();k++){
                if (bookingList.get(i).getPassengerList().get(k).getName().equals(name)&& bookingList.get(i).getPassengerList().get(k).getSurname().equals(surname)){
                    passengersFlights.add(bookingList.get(i));}
            }
        }
        return passengersFlights;
    }

    public User findUser(String name, String surname){
        List <User> users = new ArrayList<>();
        for (int i=0; i<bookingList.size();i++){
            for(int k =0; k< bookingList.get(i).getPassengerList().size();k++){
                if (bookingList.get(i).getPassengerList().get(k).getName().equals(name)&& bookingList.get(i).getPassengerList().get(k).getSurname().equals(surname)){
                    users.add(bookingList.get(i).getPassengerList().get(k));}
            }
        }
        if(users.size()>0){
            return users.get(0);}
        else {return  null; }
    }

}
