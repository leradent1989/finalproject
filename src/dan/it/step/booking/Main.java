package dan.it.step.booking;

import dan.it.step.booking.domain.Booking;
import dan.it.step.booking.domain.FlightListCreator;
import dan.it.step.booking.service.BookingService;
import dan.it.step.booking.service.FlightsService;
import dan.it.step.booking.dao.BookingDao;

import dan.it.step.booking.dao.FlightsDao;



import java.nio.file.Paths;
import java.text.ParseException;
import java.util.*;

public class Main {

    public static void main(String[] args) throws ParseException {



        FlightsDao flightsDao = new FlightsDao(FlightListCreator.getData());
        FlightsService flightsService = new FlightsService(flightsDao);

        List<Booking> bookings = new ArrayList<>();
        BookingDao bookingDao = new BookingDao(bookings);
        BookingService bookingService= new BookingService(bookingDao);

        FlightsControllerForUser flightsControllerForUser = new FlightsControllerForUser(bookingService,flightsService);


        flightsControllerForUser.start();
    }


}
