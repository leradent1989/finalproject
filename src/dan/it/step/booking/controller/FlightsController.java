package dan.it.step.booking.controller;

import dan.it.step.booking.domain.Flight;
import dan.it.step.booking.service.FlightsService;

import java.time.LocalDate;
import java.util.List;

public class FlightsController {

    private FlightsService flightsService;

    public FlightsController(FlightsService flightsService) {
        this.flightsService = flightsService;

    }
    public  void  loadData(){

        flightsService.loadData();
    }
    public  List <Flight> getFlights (){

        return  flightsService.getFlights();
    }
    public Flight findFlightById(int id){

        return     flightsService.findFlightById(id);
    }
    public void addFlight(Flight flight) {

        flightsService.addFlight(flight);
    }

    public List<Flight> findAll() {

        return flightsService.findAll();
    }

    public Flight findFlight(LocalDate date, String destination, int passengersCount){

        return flightsService.findFlight(date,destination,passengersCount );
    }

    public void showAllFlightsNext24Hours(List<Flight> flights){

        flightsService.showAllFlightsNext24Hours(flights);
    }
    public List<Flight> findFlights(LocalDate date,String destination,int passengersCount) {

        return   flightsService.findFlights(date,destination, passengersCount);
    }
}


