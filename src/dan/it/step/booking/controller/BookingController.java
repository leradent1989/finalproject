package dan.it.step.booking.controller;

import dan.it.step.booking.domain.Booking;
import dan.it.step.booking.domain.Flight;
import dan.it.step.booking.domain.User;
import dan.it.step.booking.service.BookingService;

import java.util.List;

public class BookingController {

    private BookingService bookingService;

    public BookingController(BookingService bookingService) {
        this.bookingService =bookingService;

    }
    public  void  loadData(){
        bookingService.loadData();
    }
    public  List <Booking> getBookingList (){
        return bookingService.getBookingList();
    }
    public Booking addNewBooking(List<User> passengers ,Flight flight){
     return   bookingService.addBooking(passengers,flight);
    }

    public void deleteBooking(int bookingId) {
        bookingService.deleteBooking(bookingId);
    }
    public List <Booking> getBookings (){
        return bookingService .getBookings();
    }
    public void displayAllBookings(User passenger) {

        bookingService.displayAllBookings( passenger);
    }
}
