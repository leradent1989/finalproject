package dan.it.step.booking;

import dan.it.step.booking.domain.*;

import dan.it.step.booking.service.BookingService;
import dan.it.step.booking.service.FlightsService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

public class FlightsControllerForUser {
    private BookingService bookingService;
    private FlightsService flightsService;

    public FlightsControllerForUser(BookingService bookingService, FlightsService flightsService) {
        this.bookingService = bookingService;
        this.flightsService = flightsService;
    }

    public void start(){


        bookingService.saveBookingList(new BookingsCreator(flightsService).getData());

        while (true) {
            System.out.println("Menu: ");
            System.out.println("1. Show flights schedule(next 24 hours)");
            System.out.println("2. Find flight by id");
            System.out.println("3. Add new reservation");
            System.out.println("4. Cancel reservation");
            System.out.println("5. My flights");
            System.out.println("6.Exit");
            System.out.println("You choice: ");
            Scanner scanner = new Scanner(System.in);
            if (!scanner.hasNextInt()) {
                System.out.println("Illegal input");
                continue;
            }
            int menuItem = scanner.nextInt();

            switch (menuItem) {

                case 1:
                    try {
                        List <Flight> allFlights =  flightsService.findAll();
                        if (allFlights.size() == 0){ throw  new  Exception();}
                        flightsService.showAllFlightsNext24Hours(allFlights); }
                    catch (Exception e){
                        System.err.println("LIST IS EMPTY PLEASE LOAD DATA ");
                    }
                    break;
                case 2:
                    System.out.println("Enter number of flight");
                    scanner.nextLine();
                    if (!scanner.hasNextInt()) {
                        System.out.println("Illegal input");
                        continue;
                    }
                    int id = scanner.nextInt();
                    Flight requestedFlight = flightsService.findFlightById(id);
                    if(requestedFlight==null){
                        System.out.println("There is no flight with flight number: "+id);
                        continue;
                    }
                    System.out.println(requestedFlight);
                    break;
                case 3:
                    List<String> destinations = Stream.of(Destination.values())
                            .map(Enum::name)
                            .toList();
                    System.out.println(destinations);
                    scanner.nextLine();
                    System.out.println("Please enter destination.Possible options: Washington, Antalya, Amsterdam, London, Paris. To exit from creating booking enter 0");
                    String userDestination = scanner.nextLine().trim().toUpperCase();
                    if (userDestination.equals("0")) continue;
                    boolean isDestination = destinations.contains(userDestination);
                    if(!isDestination){
                        System.out.println("Entered destination is not on the list. Please start again.");
                        continue;
                    }
                    System.out.println("Please enter date in format dd/mm/yyyy. To exit from creating booking enter 0");
                    String userDate = scanner.nextLine().trim();
                    if (userDate.equals("0")) continue;
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    format.setLenient(false);
                    try {
                        format.parse(userDate);
                    } catch (ParseException ignored) {
                        System.out.println("Entered value of date \'"+userDate+"\' is incorrect. Please start again.");
                    }
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                    LocalDate userLocalDate = LocalDate.parse(userDate, formatter);
                    System.out.println("Enter number of passengers: ");
                    if (!scanner.hasNextInt()) {
                        System.out.println("Illegal input. Please start again");
                        continue;
                    }
                    int numberOfPassenegers = scanner.nextInt();
                    List<Flight> possibleFlights = flightsService.findFlights(userLocalDate,userDestination,numberOfPassenegers);
                    if(possibleFlights==null){
                        System.out.println("Sorry, there is no such flights for this date. ");
                        continue;
                    }
                    for(int i=1; i<=possibleFlights.size();i++){
                        System.out.println("Option "+i+": "+possibleFlights.get(i-1));
                    }
                    System.out.println("Enter  flight number, or \'0\' to exit from the booking");
                    if (!scanner.hasNextInt()) {
                        System.out.println("Illegal input. Please start again");
                        continue;
                    }
                    int userChosenFlightLine = scanner.nextInt();
                    scanner.nextLine();
                    if(userChosenFlightLine>flightsService.findAll().size()){
                        System.out.println("Your choice is out of list. Please start again.");
                        continue;
                    }
                    Flight userChosenFlight = flightsService.findFlightById(userChosenFlightLine);
                    List<User> passengers = new ArrayList<>();
                    String userName;
                    String userSurname;
                    for(int i=1; i<=numberOfPassenegers; i++){
                        System.out.println("Enter name for passenger "+i+" or \'0\' to exit from the booking");
                        userName = scanner.nextLine().trim();
                        if (userName.equals("0")) continue;
                        System.out.println("Enter surname for passenger "+i+" or \'0\' to exit from the booking");
                        userSurname = scanner.nextLine().trim();
                        if (userSurname.equals("0")) continue;
                        if(bookingService.findUser(userName,userSurname) == null){
                            passengers.add(new User(userName,userSurname,new ArrayList<>()));
                        }else{
                            passengers.add(bookingService.findUser(userName,userSurname));
                        }

                    }
                    bookingService.addBooking(passengers,userChosenFlight);
                    break;
                case 4:
                    System.out.println("Please enter booking number");
                    scanner.nextLine();
                    if (!scanner.hasNextInt()) {
                        System.out.println("Illegal input. Please start again");
                        continue;
                    }
                    int userBookingNumber = scanner.nextInt();
                    boolean isDeleted = bookingService.deleteBooking(userBookingNumber);
                    if(isDeleted){
                        System.out.println("Your reservation has been deleted.");
                    } else {
                        System.out.println("Booking"+userBookingNumber+"doesn't exist.");
                    }
                    break;

                case 5:
                    System.out.println("Please enter your name");
                    scanner.nextLine();
                    String name = scanner.nextLine();
                    System.out.println("Please enter your surname");
                    String surname = scanner.nextLine();
                    List<Booking> usersFlightsFromBookings= bookingService.findAllFlightByNameAndSurname(name, surname);
                    if(usersFlightsFromBookings==null){
                        System.out.println(name+" "+surname+" is not on the passengers list. No flight found.");
                    } else {
                    usersFlightsFromBookings.forEach(el -> System.out.println(el));
                    }
                    break;
                case 6:
                    flightsService.loadData();
                    bookingService.loadData();
                    System.exit(0);
                    break;
                default:
                    System.out.println("Illegal input");
            }
        }

    }
}
