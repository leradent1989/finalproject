package dan.it.step.booking.domain;

import java.util.*;
import java.util.stream.Collectors;

public class User implements  java.io.Serializable{
   private   String name;
   private   String surname;
  private   List <Booking> booking;

    public User   (String name,String surname,List <Booking> reservation){
        this.name = name;
        this.surname = surname;
        this.booking = reservation;
    }

    public  String getName(){
        return  name;
    }
    public void setName(String name){
        this.name= name;
    }
    public String getSurname(){
        return surname;
    }
    public  void setSurname(String surname){
        this.surname =surname;
    }

    public List <Booking> getBooking(){
        return booking;
    }
    public void setBooking(List<Booking> booking){
        this.booking = booking;
    }

    @Override
    public String toString(){
        return "[ Name:" + name + " Surname: " + surname + "]";}
   @Override
    public int hashCode(){
        int  result = this.getSurname() == null?0:this.getSurname().hashCode();
        result = result +  this.getName().hashCode();
        return result;
    }
    @Override
    public boolean equals(Object obj){

        if(obj == null){
            return  false;
        }
        if(!(obj.getClass() == User.class)){
            return false;
        }
       User user = (User) obj;
        String humanName = user.getName();
        String humanSurname = user.getSurname();

        if((humanName == this.name  || humanName.equals(this.name)) &&
                ( humanSurname == this.surname ||  humanSurname.equals(this.surname))) {
            return true;
        }else  return false;

    }



    public  static User findUserInList(String name,String surname,List <User> userList){

           List <User> users = userList.stream()

                    .filter(el-> el.getName().equals(name)&& el.getSurname().equals(surname)).collect(Collectors.toList());
          if(users.size() > 0){
            return users.get(0);}else return  null;
    }


    }


