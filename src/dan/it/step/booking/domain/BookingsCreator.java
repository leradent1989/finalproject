package dan.it.step.booking.domain;

import dan.it.step.booking.service.FlightsService;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class BookingsCreator {
    private static FlightsService flightsService;

    public BookingsCreator(FlightsService flightsService) {
        this.flightsService = flightsService;
    }

    public static List<Booking> getData(){
        List <Booking> bookings;
        if (!Paths.get("listBookings").toFile().exists()) {
            generateBookings();
        }
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("listBookings") ) ) {
            bookings = (List<Booking>) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return bookings;
    }
    public static void generateBookings(){
        if (!Paths.get("listBookings").toFile().exists()) {
            List<Booking> bookings1 = new ArrayList<>();
            List<Booking> bookings2 = new ArrayList<>();
            List<Booking> bookings3 = new ArrayList<>();

            List<User> users = new ArrayList<>(List.of(
                    new User("Valeriya", "Vitvytska", bookings1),
                    new User("Roman", "Litvinenko", bookings2),
                    new User("Olga", "Kyriliuk", bookings3)
            ));
            Booking booking1 = new Booking(users, users.size(), flightsService.findFlightById(54));
            Booking booking2 = new Booking(users, users.size(), flightsService.findFlightById(28));
            Booking booking3 = new Booking(users, users.size(), flightsService.findFlightById(101));
            Booking booking4 = new Booking(users, users.size(), flightsService.findFlightById(126));

            List<Booking> bookingList = new ArrayList<>();
            bookings1.add(booking1);
            bookings1.add(booking3);
            bookings2.add(booking2);
            bookings2.add(booking4);
            bookings3.add(booking4);
            users.get(0).setBooking(bookings1);

            users.get(1).setBooking(bookings2);
            users.get(2).setBooking(bookings3);
            bookingList.add(booking1);
            bookingList.add(booking2);
            bookingList.add(booking3);
            try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("listBookings"))) {
                objectOutputStream.writeObject(bookingList);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
