package dan.it.step.booking.domain;

public enum Destination {
    PARIS,
    AMSTERDAM,
    ANTALYA,
    LONDON,
    WASHINGTON
}
