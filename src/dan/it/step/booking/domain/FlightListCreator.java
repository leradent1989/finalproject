
package dan.it.step.booking.domain;

import java.io.*;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FlightListCreator {

    public static void generate(){
        List <Flight> flights = new ArrayList<>();

        Random random = new Random();

        Destination[] destination2 = Destination.values();
        Time[] time = Time.values();
        Month[] month = Month.values();
        for(int i = 0;i<=1000;i++){

            int freeSEATS = random.nextInt(0,100);
            int dayOFMonth = random.nextInt(1,29);
            int  destination1= random.nextInt(0,5);
            int hour =random.nextInt(0,4);
            int monthRand =random.nextInt(0,3);


            flights.add(new Flight(LocalDate.of(2023,month[monthRand].count,dayOFMonth), LocalTime.of(time[hour].count,50),destination2[destination1].name(),freeSEATS,i));
        }
        for(int k = 1;k<=28;k++){
            int freeSEATS = random.nextInt(0,100);
            int dayOFMonth = random.nextInt(1,28);

            int hour =random.nextInt(0,3);
            int monthRand =random.nextInt(0,2);

            flights.add(new Flight(LocalDate.of(2023,month[monthRand].count,k), LocalTime.of(time[hour].count,50),"PARIS",freeSEATS,k +1000));
            flights.add(new Flight(LocalDate.of(2023,month[monthRand].count,k), LocalTime.of(time[hour].count,50),"LONDON",freeSEATS,k+1028));
            flights.add(new Flight(LocalDate.of(2023,month[monthRand].count,k), LocalTime.of(time[hour].count,50), "AMSTERDAM",freeSEATS,k+ 1056));
            flights.add(new Flight(LocalDate.of(2023,month[monthRand].count,k), LocalTime.of(time[hour].count,50),"ANTALYA",freeSEATS,k+1080));
            flights.add(new Flight(LocalDate.of(2023,month[monthRand].count,k), LocalTime.of(time[hour].count,50),"WASHINGTON",freeSEATS,k+1108));


        }
        for(int j = 1;j<=5;j++) {
            int freeSEATS = random.nextInt(0, 100);
            int hour = random.nextInt(0, 3);

            flights.add(new Flight(LocalDate.of(2023, 01, 30), LocalTime.of(time[hour].count, 50), "PARIS", freeSEATS, j + 1137));
            flights.add(new Flight(LocalDate.of(2023, 01, 30), LocalTime.of(time[hour].count, 50), "LONDON", freeSEATS, j + 1142));
            flights.add(new Flight(LocalDate.of(2023, 01, 30), LocalTime.of(time[hour].count, 50), "AMSTERDAM", freeSEATS, j + 1147));
            flights.add(new Flight(LocalDate.of(2023, 01, 30), LocalTime.of(time[hour].count, 50), "ANTALYA", freeSEATS, j + 1152));
            flights.add(new Flight(LocalDate.of(2023, 01, 30), LocalTime.of(time[hour].count, 50), "WASHINGTON", freeSEATS, j + 1157));
            flights.add(new Flight(LocalDate.of(2023, 01, 31), LocalTime.of(time[hour].count, 50), "PARIS", freeSEATS, j + 1162));
            flights.add(new Flight(LocalDate.of(2023, 01, 31), LocalTime.of(time[hour].count, 50), "LONDON", freeSEATS, j+ 1167));
            flights.add(new Flight(LocalDate.of(2023, 01, 31), LocalTime.of(time[hour].count, 50), "AMSTERDAM", freeSEATS, j + 1172));
            flights.add(new Flight(LocalDate.of(2023, 01, 31), LocalTime.of(time[hour].count, 50), "ANTALYA", freeSEATS, j + 1177));
            flights.add(new Flight(LocalDate.of(2023, 01, 31), LocalTime.of(time[hour].count, 50), "WASHINGTON", freeSEATS, j + 1182));
        }

        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("listFlights"))) {
            objectOutputStream.writeObject(flights);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
    public  static List <Flight> getData(){

        if (  !Paths.get("listFlights").toFile().exists()) {
            generate();
        }

        List <Flight> flights;
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("listFlights") ) ) {
            flights = (List<Flight>) objectInputStream.readObject();

        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return flights;
    }
}
