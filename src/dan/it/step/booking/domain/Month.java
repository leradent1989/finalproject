package dan.it.step.booking.domain;

public enum Month {
   MONTH_01(1),
   MONTH_02(2),
    MONTH_03(3);

    public final int count;


    Month(int count) {
        this.count = count;
    }
}
