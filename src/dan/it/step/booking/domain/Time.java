package dan.it.step.booking.domain;

public enum Time {
    TIME_10(10),
    TIME_20(17),
    TIME_30(8),
    TIME_40(12);

    public final int count;


    Time(int count) {
        this.count = count;
    }

}
