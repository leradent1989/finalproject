package dan.it.step.booking.service;

import dan.it.step.booking.domain.Booking;
import dan.it.step.booking.domain.Flight;
import dan.it.step.booking.domain.User;
import dan.it.step.booking.dao.BookingDao;

import java.util.List;
import java.util.stream.Collectors;

public class BookingService {

    private BookingDao bookingDao;

    public BookingService(BookingDao bookingDao) {
        this.bookingDao = bookingDao;
    }
 public List <Booking> getBookings(){
        return bookingDao.findAll();
 }

    public  void  loadData(){
        bookingDao.loadData();
    }
    public  List <Booking> getBookingList (){
        return bookingDao.getBookingList();
    }

     public Booking addBooking(List <User > passengers, Flight flight) {
        return bookingDao.addBooking(passengers,flight);
    }

   public void saveBookingList(List<Booking> bookings){
        bookingDao.saveBookingList(bookings);
    }

    public boolean deleteBooking(int bookingId) {
      return bookingDao.deleteBooking(bookingId);
    }

    public void displayAllBookings(User passenger) {
        List <Booking> bookings = bookingDao.findAll();
        List <Booking>  userBookings = bookings.stream().filter(el -> el.getPassengerList().contains(passenger)).collect(Collectors.toList());
        userBookings.forEach(el -> System.out.println(el.toString()));
    }

    public List<Booking> findAllFlightByNameAndSurname(String name, String surname){
        return bookingDao.checkPassenegersFlighs(name, surname);
    }

   public User findUser(String name,String surname){

        return  bookingDao.findUser(name,surname);
}

}
